# Multi-Host Laradock

## Initial install

### MySQL

For now, I don't know how to get mysql to use the my.cnf after it has first been initiated, so the `default_time_zone` entry needs to be removed from `./mysql/conf.d/my.cnf`.

If you forget to remove the `default_time_zone` entry, and the containers have already started, you will need to delete the `./data/mysql` folder, remove the `default_time_zone` entry, and run `docker-compose down && docker-compose up -d`.

---

Also, you will need to set up the `homestead` user to have all of the appropriate "global" privileges so you don't have to update permissions for each new database.
